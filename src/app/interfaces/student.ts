export interface Student {
    id?:string,
    name:string,
    math:number,
    psych:number,
    pay:boolean,
    result?:string,
    saved?:boolean,
    email?:string,
    time?:number
    

}