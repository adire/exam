import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {

  private url = "https://l04uo8r7ua.execute-api.us-east-1.amazonaws.com/adire";

  predict(pay:boolean,math:number,psych:number){
    let json = {'students':[
      {'pay':pay,'math':math,'psych':psych }
    ]}
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[',''); 
        final = final.replace(']','');
        return final; 
      })
    )

  }

  constructor(private http:HttpClient) { }
}
