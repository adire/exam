import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Student } from '../interfaces/student';

@Component({
  selector: 'studentform',
  templateUrl: './students-form.component.html',
  styleUrls: ['./students-form.component.css']
})
export class StudentsFormComponent implements OnInit {

  @Input() name:string;
  @Input() math:number;
  @Input() psych:number;
  @Input() id:string;
  @Input() pay:boolean;
  @Input() formType:string;
  @Output() update = new EventEmitter<Student>()
  @Output() closeEdit = new EventEmitter<null>()
  isError:boolean = false;
  

  tellParentToClose(){
    this.closeEdit.emit();
  }

  updateParent(){
    let student:Student = {id:this.id, name:this.name, math:this.math, psych:this.psych,pay:this.pay};
    if(this.math < 0 || this.math > 100 ||this.psych < 0 || this.psych > 800){
      this.isError = true;
    }

    else if(this.psych < 0 || this.psych > 800){
      this.isError = true;
    }

  
    else{
    this.update.emit(student);
    if(this.formType == "Add Student"){
      this.name = null;
      this.math = null;
      this.psych = null;
      this.pay = null;
    }
   }
  }

  constructor() { }

  ngOnInit(): void {
  }

}
