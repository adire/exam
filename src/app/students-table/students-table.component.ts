import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { StudentsService } from '../students.service';

@Component({
  selector: 'studentstable',
  templateUrl: './students-table.component.html',
  styleUrls: ['./students-table.component.css']
})
export class StudentsTableComponent implements OnInit {

  displayedColumns: string[] = ['name','math', 'psych', 'pay','email','time'];
  students$;
  userId:string;
  students:Student[];

  constructor(public authService:AuthService, private studentsService:StudentsService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.students$ = this.studentsService.getStudents(this.userId);
  }

    )
}

}
