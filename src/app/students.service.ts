import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  addStudent(userId:string, name:string, math:number, psych:number, pay:boolean){
    const student = {name:name, math:math, psych:psych, pay:pay};
    this.userCollection.doc(userId).collection('students').add(student)
  }

  public getStudents(userId){
    this.studentCollection = this.db.collection(`users/${userId}/students`, 
    ref => ref.limit(10)); 
    return this.studentCollection.snapshotChanges()      
  }

  public deleteStudent(userId:string , id:string){
    this.db.doc(`users/${userId}/students/${id}`).delete();
  }

  updateResult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/students/${id}`).update(
      {
        result:result
      }
    )
  }

  constructor(private db:AngularFirestore) { }
}
