import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { PredictionService } from '../prediction.service';
import { StudentsService } from '../students.service';

@Component({
  selector: 'students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students:Student[];
  students$;
  addStudentFormOpen = false;
  userId:string;
  panelOpenState = false;
  rowToDelete:number = -1;
  rowToPredict:number = -1;
  editstate = [];

  moveToDeleteState(i){
    this.rowToDelete = i; 
  }

  moveToPredictState(i){
    this.rowToPredict = i; 
  }

  add(student:Student){
    this.studentsService.addStudent(this.userId, student.name, student.math, student.psych,student.pay);
  }

  deleteStudent(id:string){
    this.studentsService.deleteStudent(this.userId,id);
    this.rowToDelete = null;
  }

  updateResult(i){
    this.students[i].saved = true; 
    this.studentsService.updateResult(this.userId,this.students[i].id,this.students[i].result);
  }

  predict(i){
    console.log(this.students[i]);
    this.predictionService.predict(this.students[i].pay, this.students[i].math,this.students[i].psych).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'finish';
        } else {
          var result = 'drop down'
        }
        this.students[i].result = result;
        console.log(result);
        this.rowToPredict = null;
      
      }
    ); 
        
      }

  constructor(public authService:AuthService, private studentsService:StudentsService,private predictionService:PredictionService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.students$ = this.studentsService.getStudents(this.userId);

        this.students$.subscribe(
          docs =>{
            
            this.students = [];
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              if(student.result){
                student.saved = true; 
              }
              student.id = document.payload.doc.id; 
              this.students.push(student); 
            }
          }
        )


      }
    )



      }

}
