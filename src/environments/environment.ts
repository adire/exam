// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAD6FfAEjGSG0Xh5jSbb00ii32nfGitI8s",
    authDomain: "exam-adire.firebaseapp.com",
    projectId: "exam-adire",
    storageBucket: "exam-adire.appspot.com",
    messagingSenderId: "514395986861",
    appId: "1:514395986861:web:ceed131f82551662a55f24"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
